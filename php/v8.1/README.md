# PHP 8.1

## Español

Esta es la configuración para PHP 8.1 que incluye los siguientes paquetes adicionales:

## English

This is the configuration for PHP 8.1 that includes the following additional packages:

## :gear: Packages

* [Composer LTS](https://getcomposer.org/)
* [PHP_CodeSniffer 3.7.2](https://github.com/squizlabs/PHP_CodeSniffer)
* [phpstan/phpstan 1.10.37](https://github.com/phpstan/phpstan)
* [phpunit/phpunit 9.6](https://phpunit.de/)
