# Pre-commit

## Español

En este repositorio encontrarás configuraciones para pre-commits en diversos lenguajes y frameworks. Todos ellos se basarán en contenedores Docker.

## English

In this repository, you will find configurations for pre-commits in various languages and frameworks. All of them will be based on Docker containers.
